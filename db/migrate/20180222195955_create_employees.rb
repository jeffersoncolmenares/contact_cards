class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :lastname
      t.integer :dni
      t.string :email
      t.string :bank
      t.string :account
      t.string :account_type
      t.string :cellphone
      t.string :phone
      t.string :phone2
      t.references :deparment, foreign_key: true

      t.timestamps
    end
  end
end
