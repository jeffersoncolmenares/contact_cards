json.extract! employee, :id, :name, :lastname, :dni, :email, :bank, :account, :account_type, :cellphone, :phone, :phone2, :deparment_id, :created_at, :updated_at
json.url employee_url(employee, format: :json)
