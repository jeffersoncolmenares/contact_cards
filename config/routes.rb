Rails.application.routes.draw do
  
  devise_for :views
  resources :departments do
    resources :employees
  end
  devise_for :companies
  get 'deparment/index'

  get 'home/index'
  
  root to: 'departments#index'
end
